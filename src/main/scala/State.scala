case class State(position: Position, direction: Direction) {

  def moveAll(grid: Grid, commands: List[Command]): State = {
    commands.foldLeft(this)((state, command) => state.move(grid, command))
  }

  def move(grid: Grid, command: Command): State = command match {
    case Command.Forward => State(moveForward(grid), direction)
    case Command.Clockwise => State(position, direction.turnClockwise())
    case Command.Anticlockwise => State(position, direction.turnAntiClockwise())
  }

  def moveForward(grid: Grid): Position = {
    val newX = position._1 + direction.getVector._1 + grid.width
    val newY = position._2 + direction.getVector._2 + grid.height
    (newX % grid.width, newY % grid.height)
  }

  def pathTo(grid: Grid, to: Position): List[Command] = {
    val delta = shortestPath(grid, to)
    var commands: List[Command] = List()

    if (delta._1.sign == direction.getVector._1) {
      // do x movements then y
      commands = commands.:++(doXMovements(delta, direction))
      val newDirection = moveAll(grid, commands).direction
      commands = commands.:++(doYMovements(delta, newDirection))
    } else {
      // do y movements then x
      commands = commands.:++(doYMovements(delta, direction))
      val newDirection = moveAll(grid, commands).direction
      commands = commands.:++(doXMovements(delta, newDirection))
    }

    commands
  }

  def faceCorrectX(delta: (Int, Int), dir: Direction): List[Command] = dir match {
    case Left if delta._1.sign == 1 => List(Command.Clockwise, Command.Clockwise)
    case Down if delta._1.sign == 1 => List(Command.Anticlockwise)
    case Down if delta._1.sign == -1 => List(Command.Clockwise)
    case Up if delta._1.sign == 1 => List(Command.Clockwise)
    case Up if delta._1.sign == -1 => List(Command.Anticlockwise)
    case Right if delta._1.sign == -1 => List(Command.Clockwise, Command.Clockwise)
    case _ => List()
  }

  def faceCorrectY(delta: (Int, Int), dir: Direction): List[Command] = dir match {
    case Down if delta._2.sign == 1 => List(Command.Anticlockwise, Command.Anticlockwise)
    case Left if delta._2.sign == 1 => List(Command.Clockwise)
    case Left if delta._2.sign == -1 => List(Command.Anticlockwise)
    case Right if delta._2.sign == 1 => List(Command.Anticlockwise)
    case Right if delta._2.sign == -1 => List(Command.Clockwise)
    case Up if delta._2.sign == -1 => List(Command.Clockwise, Command.Clockwise)
    case _ => List()
  }

  def doYMovements(delta: (Int, Int), dir: Direction): List[Command] = {
    faceCorrectY(delta, dir) ++ List.fill(delta._2.abs)(Command.Forward)
  }

  def doXMovements(delta: (Int, Int), dir: Direction): List[Command] = {
    faceCorrectX(delta, dir) ++ List.fill(delta._1.abs)(Command.Forward)
  }

  def shortestPath(grid: Grid, to: Position): (Int, Int) = {
    (shortestDistance(position._1, to._1, grid.width), shortestDistance(position._2, to._2, grid.height))
  }

  private def shortestDistance(from: Int, to: Int, limit: Int): Int = {
    val dNoWrap = to - from
    val dPosWrap = to + limit - from
    val dNegWrap = to - limit - from
    List(dNoWrap, dPosWrap, dNegWrap).minBy(_.abs)
  }

}

enum Command:
  case Forward
  case Clockwise
  case Anticlockwise

type Position = (Int, Int)
