trait Direction {
  def turnClockwise(): Direction
  def turnAntiClockwise(): Direction
  def getVector: (Int, Int)
}

object Up extends Direction {
  override def turnClockwise(): Direction = Right
  override def turnAntiClockwise(): Direction = Left
  override def getVector: (Int, Int) = (0, 1)
}

object Left extends Direction {
  override def turnClockwise(): Direction = Up
  override def turnAntiClockwise(): Direction = Down
  override def getVector: (Int, Int) = (-1, 0)
}

object Right extends Direction {
  override def turnClockwise(): Direction = Down
  override def turnAntiClockwise(): Direction = Up
  override def getVector: (Int, Int) = (1, 0)
}

object Down extends Direction {
  override def turnClockwise(): Direction = Left
  override def turnAntiClockwise(): Direction = Right
  override def getVector: (Int, Int) = (0, -1)
}
