import org.scalatest.*
import flatspec.*
import matchers.*

import scala.List

class StateSpec extends AnyFlatSpec with should.Matchers {

  private val grid: Grid = Grid(10, 10)

  "A State" should "allow forward movement" in {
    val state = State(position = (0, 0), Up)
    val newState = state.move(grid, Command.Forward)
    newState.position should be (0, 1)
  }

  it should "move right" in {
    val state = State(position = (1, 0), Right)
    val newState = state.move(grid, Command.Forward)
    newState.position should be (2, 0)
  }

  it should "move left" in {
    val state = State(position = (1, 0), Left)
    val newState = state.move(grid, Command.Forward)
    newState.position should be (0, 0)
  }

  it should "move down" in {
    val state = State(position = (1, 1), Down)
    val newState = state.move(grid, Command.Forward)
    newState.position should be (1, 0)
  }

  it should "wrap around the grid" in {
    val state = State((9, 9), Up)
    val newState = state.move(grid, Command.Forward)
    newState.position should be (9, 0)
  }

  it should "maintain direction upon moving forward" in {
    val state = State((0, 0), direction = Up)
    val newState = state.move(grid, Command.Forward)
    newState.direction should be (Up)
  }

  it should "allow clockwise direction change" in {
    val state = State((0, 0), direction = Up)
    val newState = state.move(grid, Command.Clockwise)
    newState.direction should be (Right)
    newState.position should be (0, 0)
  }

  it should "allow anti-clockwise direction change" in {
    val state = State((0, 0), direction = Up)
    val newState = state.move(grid, Command.Anticlockwise)
    newState.direction should be (Left)
    newState.position should be (0, 0)
  }

  it should "find the shortest path without wrapping" in {
    val state = State((0, 0), direction = Up)
    val shortest = state.shortestPath(grid, (5, 5))
    shortest should be (5, 5)
  }

  it should "find the shortest path with wrapping" in {
    val state = State((8, 8), direction = Up)
    val shortest = state.shortestPath(grid, (1, 1))
    shortest should be (3, 3)
  }

  it should "find the shortest path with wrapping in negative direction" in {
    val state = State((1, 1), direction = Up)
    val shortest = state.shortestPath(grid, (8, 8))
    shortest should be (-3, -3)
  }

  "A path" should "move straight up" in {
    val state = State((0, 0), Up)
    val path = state.pathTo(grid, (0, 1))
    path should be (List(Command.Forward))
  }

  it should "lead to the target point" in {
    var state = State((0, 0), Up)
    val target = (3, 3)
    val path = state.pathTo(grid, target)
    for (step <- path) {
      state = state.move(grid, step)
    }
    state.position should be (target)
  }

  it should "lead to the target point for any point on a 10x10 grid" in {
    val width = 10
    val height = 10
    for (x <- 0 until width)
      for (y <- 0 until height)
        for (d <- List(Up, Down, Left, Right)) {
          val state = State((3, 3), d)
          val target = (x, y)
          val path = state.pathTo(grid, target)
          val newPosition = state.moveAll(grid, path).position
          newPosition should be (target)
        }
  }

}